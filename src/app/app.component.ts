import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
   single = [
    {
      name: "Germany",
      series: [
        {
          value: 0,
          name: new Date(1994,9,25).toDateString()
        },
        {
          value: 50,
          name: new Date().toDateString()
        },
        {
          value: 70,
          name: new Date(2018,8,24).toDateString()
        }
      ]
     
    }
  ];
  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };
}
